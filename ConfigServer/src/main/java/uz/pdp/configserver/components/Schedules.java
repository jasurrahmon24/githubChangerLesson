package uz.pdp.configserver.components;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

@Component
@EnableScheduling
public class Schedules {
    boolean bool = true;
    public final String ABS_PATH= "D:\\Teaching\\BootcampGroups\\b21\\SPRING_3\\configServer\\ConfigServer\\";

//    @Scheduled(fixedDelay = 30000,initialDelay = 5000)
    public void changeVariables(){
        if (this.bool) {
            try {
                System.out.println("Scheduling...");
                changeFile("first");
                changeFile("second");
               Runtime.getRuntime().exec("cmd /c pushingToGit.bat", null, new File(ABS_PATH));
            } catch (IOException e) {
                System.out.println("Bat emadi");
                e.printStackTrace();
            }
        }
    }

    public void changeFile(String fileName) throws IOException {

        FileInputStream fileInputStream = new FileInputStream(ABS_PATH+fileName+".properties");
        Scanner scanner = new Scanner(fileInputStream);
        List<String> properties = new ArrayList<>();
        while (scanner.hasNextLine()) {
            properties.add(scanner.nextLine());
        }
        List<String> changedProperties = new ArrayList<>();
        for (int i = 0; i < properties.size(); i++) {
            String property = properties.get(i);
            if (property.substring(0,property.indexOf("=")).toLowerCase().contains(fileName)){
                property = property.substring(0, property.indexOf("=") + 1) + UUID.randomUUID();
                System.out.println(property);
            }
            changedProperties.add(i, property);
        }
        System.out.println(changedProperties);
        scanner.close();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(ABS_PATH+fileName+".properties"));
        StringBuilder str = new StringBuilder();
        for (String s : changedProperties) {
            str.append(s).append("\n");
        }
        bufferedWriter.write(str.toString());
        bufferedWriter.close();
    }
}
